import React, {Component} from 'react'

function NameList(){
    const persons = [
        {
            id: 1,
            name: "Riddhi",
            skills: "React"
        },
        {
            id: 2,
            name: "Meetraj",
            skills: "Vue"
        },
        {
            id: 3,
            name: "Isha",
            skills: "Angular"
        },
    ]
    const nameList = persons.map(person => person.name);    
    return <div> nameList </div>
}
export default NameList;