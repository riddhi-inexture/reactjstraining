import react,{Component} from 'react'
import Table from './Table'
import Form from './form'
import ParentComponent from './ParentComponent'
import NameList from './NameList'
import FragmentDemo from './FragmentDemo'
import RefDom from './RefDom'
import FocusInput from './FocusInput'
import Hooks from './Hooks'
class App extends Component{
  
  state = {
    empDetails :[
    //   {

    //   'fname': 'riddhi',
    //   'lname': 'soni',
    //   'email': 'riddhi@gm.cc',
    //   'phone': '9090909090',
    // }
    ]
  }

  removeName = (index) =>{
    const indexV = -1;
    const {empDetails} = this.state
    this.setState({
      empDetails : empDetails.filter((name,i)=>{
        return i !== index
      }),
    })
  }

  fetchState = (index) => {
    const {empDetails} = this.state
    return {empDetails}[index];
  }
  
    render(){

      const {empDetails} = this.state
      return (
        <div className = 'container'>
          <Hooks />
          <RefDom />
          <FocusInput />
          <FragmentDemo />
          <ParentComponent />
          <NameList />
        <Table names = {empDetails} removeName = {this.removeName} fetchState = {this.fetchState}/>
        <Form handleSubmit = {this.handleSubmit} fetchState = {this.fetchState}/>
        </div>
      )
    }
    handleSubmit = (name)=> {
      this.setState({empDetails:[...this.state.empDetails,name]})
    }
  }
  export default App