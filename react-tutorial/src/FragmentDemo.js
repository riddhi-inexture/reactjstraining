import React from 'react'

function FragmentDemo() {
    return(
        <React.Fragment>
            <h1> FragmentDemo </h1>
            <p> This is paragraph </p>
             </React.Fragment>
    )
}
export default FragmentDemo