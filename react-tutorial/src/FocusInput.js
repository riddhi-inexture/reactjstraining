import { Component } from "react"

import React from 'react'
import Input from "./Input";

class FocusInput extends Component{
    constructor(props){
        super(props);
        this.componenetRef = React.createRef()
    }
    clickHandler = () => {
        this.componenetRef.current.focusInput()
    }
    render(){
        return (
            <div>
                <Input ref= {this.componenetRef}/>
                <button onClick={this.clickHandler}>Focus Input</button>
                </div>
        )
    }
}
export default FocusInput