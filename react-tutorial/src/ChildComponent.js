import React , {Component} from 'react'

class ChildComponent extends Component{
    render(){
        return(
            <div>
                <button onClick={() => this.props.greetHandle("child")}> Greet Parent </button>
                </div>
        )
    }
}

export default ChildComponent