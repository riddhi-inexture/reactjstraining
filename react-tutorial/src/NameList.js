import React from 'react'
import Person from './Person'

function NameList(){
    const persons = [
        {
            id: 1,
            name: "Riddhi",
            skills: "React"
        },
        {
            id: 2,
            name: "Manali",
            skills: "Vue"
        },
        {
            id: 3,
            name: "Isha",
            skills: "Angular"
        },
    ]
    const nameList = persons.map(person => <Person key={person.id} perso={person}/>);
    return <div>{nameList} </div>
}
export default NameList;