import react,{Component} from 'react'

const TableBody = (props) => {
    // console.log(props.name);
     
    const name = props.name.map((row,index)=>{
        return(
            <tr key={index}>
                <td>{row.fname}</td>
                <td>{row.lname}</td>
                <td>{row.email}</td>
                <td>{row.phone}</td>
                <td><button onClick={()=> props.removeName(index)}>Delete</button></td>
            </tr>
        )
    })
    return <tbody>{name}</tbody>
}
const HeaderName = (props) => {
    const anmae = props.aname;
    return (
        <h1> {anmae} </h1>
    )
}
const TableHead = () => {
    return (
    <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone No</th>
          <th>Action</th>
        </tr>
      </thead>
      )
}
const Table = (props) => {
    console.log(props);
    const {names, removeName, fetchState} = props
    return(
        <table>
            <TableHead />
            <TableBody name = {names} removeName = {removeName} fetchState = {fetchState}/>
        </table>
    )
}
// class Table extends Component{
//     render(){
//         // console.log(this.props);
//         const {name, removeName} = props
//         return(
//             <div>
//           <table>
//             <TableHead />
//             <TableBody name = {name} removeName = {removeName} />
//             </table>
//             <HeaderName  aname = {this.props.name}/>
//          </div>
        
//         )
//     }
// }

export default Table