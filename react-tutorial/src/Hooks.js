import React ,{useState,useEffect }from 'react'

const Hooks = () => {
     const [state, setstate] = useState(0);

     useEffect(() => {
        document.title = `You clicked ${state} times`;
     })

     return(
         <div>
            <p> You have clicked {state} times</p>
                <button onClick={()=>setstate(state+1)}>Increase Count</button>
                <button onClick={()=>setstate(state-1)}>Decrease Count</button>
                </div>
     )
}

export default Hooks