import react,{Component} from 'react'
class Form extends Component{
    initialState = {
        'fname' : '',
        'lname' : '',
        'email' : '',
        'phone' : '',
    }
    state = this.initialState

    handleChange = (event) => {
        // console.log(event.target);
        const {name,value} = event.target
        this.setState({
            [name]:value,
        })
    }
    submitForm = () => {
        this.props.handleSubmit(this.state)
        this.setState(this.initialState)
    }
    
    render(){
        
        const {fname,lname,email,phone} = this.state
        console.log(this.indexV);
        return(
            <form>
                <label htmlFor="fname">First Name</label>
                <input
                    type="text"
                    name="fname"
                    id="fname"
                    value={fname}
                    onChange= {this.handleChange} />

                <label htmlFor="lname">Last Name</label>
                    <input
                        type="text"
                        name="lname"
                        id="lname"
                        value={lname}
                        onChange= {this.handleChange}/>

                <label htmlFor="email">Email</label>
                    <input
                        type="text"
                        name="email"
                        id="email"
                        value={email}
                        onChange= {this.handleChange}/>

                <label htmlFor="phone">Phone No.</label>
                    <input
                        type="text"
                        name="phone"
                        id="phone"
                        value={phone}
                        onChange= {this.handleChange}/>

                <input type="button" value="Submit" onClick={this.submitForm} />
            </form>
        );
    }
}
export default Form;