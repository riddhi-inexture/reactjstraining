import logo from './logo.svg';
import {useState} from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './style.css';
import {BrowserRouter as Router,Switch,Route,Link} from 'react-router-dom'
// import Login from './components/Login'
import Login from './Functional Components/Login'
// import Signup from './components/Signup'
import Signup from './Functional Components/Signup'
import Dashboard from './Functional Components/Dashboard';
import ValidateForm from './components/ValidateForm';
import Description from './Functional Components/Description';
import Pagination from './Functional Components/Pagination';
import DemoPagination from './Functional Components/DemoPagination';


function App() {
  
  return (
    <Router>
    <div className="App">
      {/* <div className = "container"> */}
        
        <Switch>
          <Route exact path='/' component={Login}/>
          <Route  path='/login' component={Login}/>
          <Route  path='/signup' component={Signup}/>
          <Route  path='/dashboard' component={Dashboard}/>
          <Route  path='/validateForm' component={ValidateForm}/>
          <Route  path='/description'  component={Description}/>
          <Route  path='/pagination'  component={Pagination}/>

          
          
        </Switch>
      {/* </div> */}
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
    </Router>
  );
  
}

export default App;
