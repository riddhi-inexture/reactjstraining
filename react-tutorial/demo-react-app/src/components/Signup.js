// import React, { Component } from  'react'

// class Signup extends Component
// {
//     userData;

//     constructor(props){
//         super(props)
//         this.state = {
//             fname:'',
//             lname:'',
//             pass:'',
//             cpass:'',
//             phone:''
//           }
//     }
//     handleChange = (event) => {

//         const {name,value} = event.target
//         this.setState({
//             [name]:value,
//         })
//     }
//     componentDidMount(){
//         this.userData = JSON.parse(localStorage.getItem('user'));
//         if(localStorage.getItem('user')){
//             this.setState({
//                 fname: this.userData.fname,
//                 lname: this.userData.lname,
//                 email: this.userData.email,
//                 pass: this.userData.pass,
//                 cpass: this.userData.cpass,
//                 phone: this.userData.phone  
//             })
//         }
//         else{
//             this.setState({
//                 fname:'',
//                 lname:'',
//                 pass:'',
//                 cpass:'',
//                 phone:''
//               });
//         }
//     }
//     componentWillUpdate(nextProps,nextState){
//         localStorage.setItem('user',JSON.stringify(nextState));
//     }
//     submitForm = (e) => {
//         e.preventDefault()
//         this.props.history.push('/login');
//     }
//     render(){
//         const {fname,
//             lname,
//             email,
//             pass,
//             cpass,
//             phone} = this.state
//         return(
//             <div className="row">
//                 <div className="col-12">
//             <form >

//             <h3 style={{textAlign:"center"}}>Sign Up</h3>

//             <div className="form-group">
//                 <label>First Name</label>
//                 <input type="text" name="fname" value={fname} className="form-control" placeholder="Enter First Name"  onChange={this.handleChange}/>
//             </div>
//             <div className="form-group">
//                 <label>Last Name</label>
//                 <input type="text" name="lname" value={lname} className="form-control" placeholder="Enter Last Name"  onChange={this.handleChange}/>
//             </div>

//             <div className="form-group">
//                 <label>Email</label>
//                 <input type="email" name="email" value={email} className="form-control" placeholder="Enter Email" onChange={this.handleChange} />
//             </div>

//             <div className="form-group">
//                 <label>Password</label>
//                 <input type="password" name="pass" value={pass} className="form-control" placeholder="Enter Password"  onChange={this.handleChange}/>
//             </div>
//             <div className="form-group">
//                 <label>Re-enter Password</label>
//                 <input type="password" name="cpass" value={cpass} className="form-control" placeholder="Enter Password"  onChange={this.handleChange}/>
//             </div>
//             <div className="form-group">
//                 <label>Contact No.</label>
//                 <input type="number" name="phone" value={phone} className="form-control" placeholder="Enter Contact No"  onChange={this.handleChange}/>
//             </div>

//             <button type="submit" onClick={this.submitForm} className="btn btn-dark btn-lg btn-block">Sign Up</button>
//             <p className="forgot-password text-right">
//                 Aleready Registerd? <a href={"/login"}>Sign In</a>
//             </p>
//         </form>
//         </div>
//         </div>
//         )
//     }
// }
// export default Signup

// import React, { Component } from  'react'
// //  import { useHistory } from "react-router-dom";

// class Login extends Component
// {
//     userData;
//     constructor(props){
//         super(props);
//         this.state = ({
//             email:'',
//             pass:''
//         })
//     }
    
//     handleSubmit = (e) => {
//         e.preventDefault();
//         this.userData = JSON.parse(localStorage.getItem('user'));
//         if(this.userData.email != ''){
//             if(this.userData.email === this.state.email && this.userData.pass === this.state.pass)
//             this.props.history.push('/dashboard');
//             console.log(this.userData);
//         }
//     }
//     handleChange = (event) => {

//         const {name,value} = event.target
//         this.setState({
//             [name]:value,
//         })
//     }
    
//     render(){
//         const {email,pass} = this.state;
//         return(
//             <div className="row">
//                 <div className="col-12">
//             <form>

//             <h3 style={{textAlign:"center"}}>Log In</h3>

//             <div className="form-group">
//                 <label>Email</label>
//                 <input type="email" className="form-control" name="email" value={email} placeholder="Enter Email" onChange={this.handleChange}/>
//             </div>

//             <div className="form-group">
//                 <label>Password</label>
//                 <input type="password" className="form-control" name="pass" value={pass} placeholder="Enter Password" onChange={this.handleChange}/>
//             </div>
//             <button type="submit" onClick={this.handleSubmit} className="btn btn-dark btn-lg btn-block">Log In</button>
//             <p className="forgot-password text-right">
//                 Not Yet Registerd? <a href={"/signup"}>Register</a>
//             </p>
//         </form>
//         </div>
//         </div>
//         )
//     }
// }
// export default Login
import React, { Component } from 'react';
import { Formik } from  'formik';
import * as EmailValidator from "email-validator";
import * as Yup from 'yup';

class Signup extends Component
{

    userData;
    constructor(props){
        super(props);
    }
    render(){
      // const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
      const phoneRegExp = /^[0-9]{10}$/ 
      const validationSchema = Yup.object().shape({
        fname: Yup.string()
          .required("Required"),
        lname: Yup.string()
          .required("Required"),
        phone: Yup.string()
          .required("Required")
          .matches(phoneRegExp,'Phone number not valid'),
        email: Yup.string()
          .email()
          .required("Required"),
        password: Yup.string()
          .required("No password provided.")
          .min(8, "Password must be of 8 chars minimum."),
        cpassword: Yup.string()
          .required("No password provided.")
          .min(8, "Password must be of 8 chars minimum.")
          .oneOf([Yup.ref('password'),null],'password must match')
      })
        return(
            <Formik
            initialValues={{ fname:"",lname:"",phone:"",email: "", password: "",cpassword: "" }}
            onSubmit={(values, { setSubmitting }) => {
                const userData = {};
                userData.fname = values.fname;
                userData.lname = values.lname;
                userData.email = values.email;
                userData.password = btoa(values.password); 
                userData.phone= values.phone;
                localStorage.setItem('user',JSON.stringify(userData));
                this.props.history.push('/login');
            }}
            // validate={values => {
            //     let errors = {};
            //     if (!values.fname) {
            //         errors.fname = "First name field is required";
            //     }
            //     if (!values.lname) {
            //     errors.lname = "Last name field is required";
            //     } 
            //     if (!values.phone) {
            //         errors.phone = "Phone numebr field is required";
            //     }else if(!Number(values.phone)){
            //         errors.phone = "Phone numebr must only digits.";
            //     }else if (values.phone.length != 10) {
            //         errors.phone = "Phone numebr must be 10 digits long.";
            //     }

            //     if (!values.email) {
            //       errors.email = " Email field is required";
            //     } else if (!EmailValidator.validate(values.email)) {
            //       errors.email = "Invalid email address.";
            //     }
            //     if (!values.password) {
            //       errors.password = "Password filed is required";
            //     } else if (values.password.length < 8) {
            //       errors.password = "Password must be 8 characters long.";
            //     }
            //     if (!values.cpassword) {
            //         errors.cpassword = "Confirm Password filed is required";
            //       } else if (values.cpassword.length < 8) {
            //         errors.cpassword = "Password must be 8 characters long.";
            //       } else if (values.cpassword !== values.password) {
            //         errors.cpassword = "Password and confirm password must be same.";
            //       }

            //     return errors;
            //   }}
            validationSchema = {validationSchema}
          >
            { props => {
              const {
                values,
                touched,
                errors,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit
              } = props;
                return (
                    <div >
                        <form onSubmit={handleSubmit}>
                            <h3 style={{textAlign:"center"}}>Sign Up</h3>
                            <label htmlFor="email">First Name</label>
                            <input
                            id="fname"
                            name="fname"
                            type="text"
                            placeholder="Enter your first name"
                            value={values.fname}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.fname && touched.fname && "error"}
                            />
                            {errors.fname && touched.fname && (
                            <div className="input-feedback">{errors.fname}</div>
                            )}
                            <label htmlFor="email">Last Name</label>
                            <input
                            id="lname"
                            name="lname"
                            type="text"
                            placeholder="Enter your last name"
                            value={values.lname}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.lname && touched.lname && "error"}
                            />
                            {errors.lname && touched.lname && (
                            <div className="input-feedback">{errors.lname}</div>
                            )}
                            <label htmlFor="email">Email</label>
                            <input
                            id="email"
                            name="email"
                            type="text"
                            placeholder="Enter your email"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.email && touched.email && "error"}
                            />
                            {errors.email && touched.email && (
                            <div className="input-feedback">{errors.email}</div>
                            )}

                            <label htmlFor="password">Password</label>
                            <input
                            id="password"
                            name="password"
                            type="password"
                            placeholder="Enter your password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.password && touched.password && "error"}
                            />
                            {errors.password && touched.password && (
                                <div className="input-feedback">{errors.password}</div>
                            )}
                             <label htmlFor="password">Re enter Password</label>
                            <input
                            id="cpassword"
                            name="cpassword"
                            type="password"
                            placeholder="Enter your confirm password"
                            value={values.cpassword}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.cpassword && touched.cpassword && "error"}
                            />
                            {errors.cpassword && touched.cpassword && (
                                <div className="input-feedback">{errors.cpassword}</div>
                            )}
                            <label htmlFor="email">Contact No</label>
                            <input
                            id="phone"
                            name="phone"
                            type="text"
                            placeholder="Enter your phone no"
                            value={values.phone}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.phone && touched.phone && "error"}
                            />
                            {errors.phone && touched.phone && (
                            <div className="input-feedback">{errors.phone}</div>
                            )}

                            <button style={{float:"right"}} type="submit" className="subBtn" disabled={isSubmitting}>
                            Register
                            </button>
                            <p className="forgot-password text-right">
                                 Already Registerd? <a href={"/login"}>Sign In</a>
                            </p>
                        </form>
                    </div>
                );
            }}
          </Formik>
            
        )
    }
}
export default Signup