// import React, { Component } from  'react'
// //  import { useHistory } from "react-router-dom";

// class Login extends Component
// {
//     userData;
//     constructor(props){
//         super(props);
//         this.state = ({
//             email:'',
//             pass:''
//         })
//     }
    
//     handleSubmit = (e) => {
//         e.preventDefault();
//         this.userData = JSON.parse(localStorage.getItem('user'));
//         if(this.userData.email != ''){
//             if(this.userData.email === this.state.email && this.userData.pass === this.state.pass)
//             this.props.history.push('/dashboard');
//             console.log(this.userData);
//         }
//     }
//     handleChange = (event) => {

//         const {name,value} = event.target
//         this.setState({
//             [name]:value,
//         })
//     }
    
//     render(){
//         const {email,pass} = this.state;
//         return(
//             <div className="row">
//                 <div className="col-12">
//             <form>

//             <h3 style={{textAlign:"center"}}>Log In</h3>

//             <div className="form-group">
//                 <label>Email</label>
//                 <input type="email" className="form-control" name="email" value={email} placeholder="Enter Email" onChange={this.handleChange}/>
//             </div>

//             <div className="form-group">
//                 <label>Password</label>
//                 <input type="password" className="form-control" name="pass" value={pass} placeholder="Enter Password" onChange={this.handleChange}/>
//             </div>
//             <button type="submit" onClick={this.handleSubmit} className="btn btn-dark btn-lg btn-block">Log In</button>
//             <p className="forgot-password text-right">
//                 Not Yet Registerd? <a href={"/signup"}>Register</a>
//             </p>
//         </form>
//         </div>
//         </div>
//         )
//     }
// }
// export default Login
import React, { Component } from 'react';
import { Formik } from  'formik';
import * as EmailValidator from "email-validator";
import * as Yup from 'yup';

class Login extends Component
{

    userData;
    constructor(props){
        super(props);
    }
    render(){
      const validationSchema = Yup.object().shape({
        email: Yup.string()
          .email()
          .required("Required"),
        password: Yup.string()
          .required("No password provivalidationSchemaded.")
          .min(8, "Password must be of 8 chars minimum.")
          // .matches(/(?=.*[0-9])/, "Password must contain a number.")
      })
        return(
            <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values, { setSubmitting }) => {
                this.userData = JSON.parse(localStorage.getItem('user'));
                
                if(localStorage.getItem('user')){
                    if(this.userData.email === values.email && atob(this.userData.password) === values.password){
                        this.props.history.push('/dashboard');
                    }
                    else{
                        // setSubmitting(true);
                        alert('Invalid credentials');
                    }
                }
                else{
                    // setSubmitting(true);
                    alert('Invalid credentials');
                }
            }}       
            validationSchema={validationSchema}
            
            // validate={values => {
            //     let errors = {};
            //     if (!values.email) {
            //       errors.email = " Email field is required";
            //     } else if (!EmailValidator.validate(values.email)) {
            //       errors.email = "Invalid email address.";
            //     }
              
            //     if (!values.password) {
            //       errors.password = "Password filed is required";
            //     } else if (values.password.length < 8) {
            //       errors.password = "Password must be 8 characters long.";
            //     }
            //     return errors;
            //   }}
          >
            {props => {
              const {
                values,
                touched,
                errors,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit
              } = props;
              return (
                <div className="formDiv">
                <form onSubmit={handleSubmit}>
                <h3 style={{textAlign:"center"}}>Log In</h3>
                    <label htmlFor="email">Email</label>
                    <input
                    id="email"
                    name="email"
                    type="text"
                    placeholder="Enter your email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={errors.email && touched.email && "error"}
                    />
                    {errors.email && touched.email && (
                    <div className="input-feedback">{errors.email}</div>
                    )}

                    <label htmlFor="password">Password</label>
                    <input
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Enter your password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={errors.password && touched.password && "error"}
                    />
                    {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                    )}

                    <button style={{float:"right"}} type="submit" className="subBtn" >
                    Login
                    </button>
                    <p className="forgot-password text-right">
                        Not Yet Registerd? <a href={"/signup"}>Register</a>
                    </p>
                </form>
                
                </div>
                
              );
            }}
          </Formik>
            
        )
    }
}
export default Login