import React, { Component } from 'react'
import ReactPaginate from 'react-paginate'
import Header from './Header'
import { Link } from 'react-router-dom'
 
class Pagination extends Component
 {
    constructor(props){
        super(props);
        this.state = {
            currentPage: 0,
            items: [],
            isLoaded: false,
            pageCount: 10
        }
    }
    fetchRecord = ({selected: selectedPage}) => {
        this.setState((prev) => { 
            return{currentPage: selectedPage};
          })
        console.log(this.state.currentPage);
        fetch(`https://hn.algolia.com/api/v1/search?query=startups&page=${this.state.currentPage}`)
        .then(res => res.json())
        .then(
            (result => {
                this.setState({
                    isLoaded:true,
                    items:result.hits
                });
            })
        )
        .catch(error => console.error('Error',error))        
        
    }

    // handlePageChange = ({selected: selectedPage}) => {
    //     console.log(selectedPage);
    // }
    render(){
        const {error,items,isLoaded} = this.state;
        if(error){
            return <div>Error: {error.message}</div>
        }
        // else if(!isLoaded){
        //     return <div>Loading...</div>
        // }
        else{
            this.userData = JSON.parse(localStorage.getItem('user'));
            if(localStorage.getItem('user')){
                const name = this.userData.fname +' '+ this.userData.lname
                return(
                    
                    <div>  
                    <Header/>
                    {/* <button onClick={this.fetchRecord}>Get Data</button> */}
                    <div style={{textAlign:'center',display:'block',marginBottom:'30px'}}><h3 >Articles</h3></div>
                        <div className="container">
                            <div className="row" style={{textAlign: "center",position:'relative'}}>
                                
                                {this.state.items.map(item => (
                                    <div className="col-4">
                                    <div className="card" style={{width:"22rem"}}>
                                    {/* <img src={item.urlToImage} class="card-img-top" alt="..." width="50" height="160"/> */}
                                    <div className="card-body ">
                                    <h5 className="card-title"><strong>{item.author}</strong></h5>
                                    <p className="card-text">{item.title}</p>
                                    <div className="cardBtnDiv">
                                    {/* <Link to={{
                                            pathname: `/description`,
                                            state: {
                                                newsItem : item
                                            }
                                        }}>
                                            <button style={{float:"right", marginBottom:"10px" ,width:"70px",borderRadius:'5px'}} class="load-more">
                                            View
                                        </button>
                                        </Link> */}
                                    {/* <a data-attr={item} href={`/description/${JSON.stringify(item.description)}`} style={{float:"right", marginBottom:"10px" }} class="btn btn-primary ">View</a>  */}
                                    </div>
                                    </div>
                                    </div>
                                </div>
                                ))}
                                 
                                    <ReactPaginate 
                                    pageCount={this.state.pageCount}
                                    pageRange={1}
                                    marginPagesDisplayed={10}
                                    onPageChange={this.fetchRecord}
                                    containerClassName={'container'}
                                    previousLinkClassName={'page'}
                                    breakClassName={'page'}
                                    nextLinkClassName={'page'}
                                    pageClassName={'page'}
                                    disabledClassName={'disabled'}
                                    activeClassName={'active'}
                                    // onPageChange={this.fetchRecord}
                                    />

                                {/* {this.state.visible < this.state.items.length &&
                                    <div style={{textAlign:"center"}}><button onClick={this.loadMore} style={{textAlign:'center',marginLeft:'15px'}} type="button" className="load-more pull-right">Load more</button></div>
                                } */}
                                    
                            </div>
                        </div>
                    </div>
                )
            }
            else{
                this.props.history.push('/login');
                window.location.reload();
            }
        }
    }

 }
 export default Pagination