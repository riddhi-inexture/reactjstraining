import React, { Component } from 'react'
import {Link} from 'react-router-dom'

class Header extends Component
{
    userData;
    constructor(props){
        super(props);
    }
    handleClick = () => {
        localStorage.removeItem('user');
        this.props.history.push('/');
        window.location.reload();
    }
    render(){
        this.userData = JSON.parse(localStorage.getItem('user'));
        let name = '';
        if(localStorage.getItem('user')){
             name = this.userData.fname +' '+ this.userData.lname;
        }            
        return(
            <header className="header-dashboard">
                    <div className="container">
                        <div className="row">
                        <div className="col-6">
                        
                            <h5 className="header-left">
                                Dashboard
                            </h5>
                        </div>
                        <div className="col-6">
                            <ul>
                                <li>
                                {name}
                                </li>
                                <li>
                                <a href={'/pagination'}>Pagination</a>
                                </li>
                                <li>
                                <a href="javascript:void(0)" onClick={this.handleClick}>Logout</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                        </div>
                        </header>
        )
    }
}
export default Header