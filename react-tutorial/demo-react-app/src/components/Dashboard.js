import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Header from './Header';

class Dashboard extends Component{
    userData;
    constructor(props){
        super(props);
        this.state={
            isLoaded:false,
            error:null,
            visible:3,
            items:[]
        };
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore(){
        this.setState((prev) => {
            return{visible: prev.visible + 3};
        });
    }
   
    componentDidMount(){
        fetch('https://newsapi.org/v2/everything?domains=wsj.com&apiKey=4f168f74fdef42a99e6c14db1528194c')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded:true,
                    items: result.articles
                });
            },
            (error) =>{
                this.setState({
                    isLoaded:true,
                    error
                });
            }
        )
    }
    render(){
        const {error,items,isLoaded} = this.state;
        if(error){
            return <div>Error: {error.message}</div>
        }
        else if(!isLoaded){
            return <div>Loading...</div>
        }
        else{
            this.userData = JSON.parse(localStorage.getItem('user'));
            if(localStorage.getItem('user')){
                const name = this.userData.fname +' '+ this.userData.lname
                return(
                    <div>  
                    <Header/>
                    <div style={{textAlign:'center',display:'block',marginBottom:'30px'}}><h3 >Articles</h3></div>
                        <div className="container">
                            <div className="row" style={{textAlign: "center",position:'relative'}}>
                                
                                {this.state.items.slice(0,this.state.visible).map(item => (
                                    <div className="col-4">
                                    <div className="card" style={{width:"22rem"}}>
                                    <img src={item.urlToImage} class="card-img-top" alt="..." width="50" height="160"/>
                                    <div className="card-body ">
                                    <h5 className="card-title"><strong>{item.author}</strong></h5>
                                    <p className="card-text">{item.title}</p>
                                    <div className="cardBtnDiv">
                                    <Link to={{
                                            pathname: `/description`,
                                            state: {
                                                newsItem : item
                                            }
                                        }}>
                                            <button style={{float:"right", marginBottom:"10px" ,width:"70px",borderRadius:'5px'}} class="load-more">
                                            View
                                        </button>
                                        </Link>
                                    {/* <a data-attr={item} href={`/description/${JSON.stringify(item.description)}`} style={{float:"right", marginBottom:"10px" }} class="btn btn-primary ">View</a>  */}
                                    </div>
                                    </div>
                                    </div>
                                    
                                </div>
                                    // <div className="col-md-3">
                                    //     <div className="designBlock">
                                    //     <h4 style={{textAlign:"center"}}>{item.author}</h4>
                                    //     <p> {item.content}</p>
                                    //     </div>
                                    // </div>
                                    
                                ))}

                                {this.state.visible < this.state.items.length &&
                                    <div style={{textAlign:"center"}}><button onClick={this.loadMore} style={{textAlign:'center',marginLeft:'15px'}} type="button" className="load-more pull-right">Load more</button></div>
                                }
                                    
                            </div>
                        </div>
                    </div>
                )
            }
            else{
                this.props.history.push('/login');
                window.location.reload();
            }
        }
    }
}
export default Dashboard

