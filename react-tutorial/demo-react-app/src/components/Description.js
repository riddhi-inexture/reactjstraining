import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Header from './Header';

class Description extends Component
{
    newsItem;
    constructor(props){
        super(props);
        this.newsItem = props.location.state.newsItem;
    }
    render(){
        return(
            <div>
            <Header />
            <div style={{textAlign:'center',display:'block',marginBottom:'30px'}}><h3>{this.newsItem.title}</h3></div>
            <div className="container">
                <div className="row">            
                <div className="card" style={{width:"50rem",marginLeft:'150px'}}>
                    <div className="card-body" style={{height:'auto'}}>
                         <img src={this.newsItem.urlToImage} class="card-img-top" alt="..." width="50" height="300"/>
                        <h5 className="card-title" style={{textAlign:"center",marginTop:"20px"}}><strong>Author Name:</strong> {this.newsItem.author}</h5>
                        <p className="card-text"><strong>Title: </strong>{this.newsItem.title}</p>
                        <p className="card-text"><strong>Description: </strong>{this.newsItem.description}</p>
                        <p className="card-text"><strong>Published At:</strong> {this.newsItem.publishedAt}</p>
                        <div className="cardBtnDiv">
                        <Link to={{
                                pathname: `/dashboard`,
                            }}>
                                <button style={{float:"right", marginBottom:"10px",width:"75px",borderRadius:'5px'}} class="load-more">
                                Back
                            </button>
                        </Link>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
export default Description