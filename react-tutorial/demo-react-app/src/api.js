import React from 'react'
import axios from 'axios'

console.log('HIiii');
const instance = axios.create({
    baseURL:'https://',
    headers: {
        Accept: 'application/json',
    }
});

instance.interceptors.request.use(
    request => {
    
        console.log("In request interceptor");
        return request;
    },
    error => {  
        return Promise.reject(error);
    }
)
instance.interceptors.response.use(
    (response) => {
        if(response.status == 200){
            console.log("In response interceptor");
        }
        return response
    },
    (error) => {
        return Promise.reject(error);
    }
)

    
export default instance
