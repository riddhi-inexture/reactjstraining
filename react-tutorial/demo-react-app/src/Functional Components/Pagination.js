import React,{useState,useEffect} from 'react'
import ReactPaginate from 'react-paginate'
import Header from '../Functional Components/Header'
import axios from '../api'
import "../styles.css";

const PER_PAGE = 20;

function Pagination(){

    const pageCount = 10;
    const [ isLoaded ,  setIsLoaded] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [data, setData] = useState([]);
    const offset = currentPage * PER_PAGE;

    useEffect(() => {
        fetchData();
      }, []);
    
      function fetchData() {
        axios.get(`hn.algolia.com/api/v1/search?query=startups&page=${currentPage}`)
        //   .then((res) => res.json())
          .then(data => {
              console.log(data);
            
            setData(data.data.hits);
          });
      }
    
      function handlePageClick({ selected: selectedPage }) {
          setCurrentPage(selectedPage);
          fetchData();
      }
    return(
        <div>  
            <Header/>
            <div style={{textAlign:'center',display:'block',marginBottom:'30px'}}><h3 >Articles</h3></div>
            <div className="container">
                <div className="row" style={{textAlign: "center",position:'relative'}}>
                    
                    {data.map(item => (
                        <div className="col-4">
                            <div className="card" style={{width:"22rem"}}>
                                <div className="card-body ">
                                    <h5 className="card-title"><strong>{item.author}</strong></h5>
                                    <p className="card-text">{item.title}</p>
                                </div>
                            </div>
                        </div>
                    ))}
                        
                        <ReactPaginate
                        previousLabel={"← Previous"}
                        nextLabel={"Next →"}
                        pageCount={pageCount}
                        onPageChange={handlePageClick}
                        containerClassName={"pagination"}
                        previousLinkClassName={"pagination__link"}
                        nextLinkClassName={"pagination__link"}
                        disabledClassName={"pagination__link--disabled"}
                        activeClassName={"pagination__link--active"}
                    />
                </div>
            </div>
        </div>    
    )

}
export default Pagination
