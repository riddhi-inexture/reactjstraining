import React from 'react'
import { Formik } from  'formik';
import { useHistory } from 'react-router-dom';
import Validate from '../Validations/validate'
 
function Signup()
{
    const history = useHistory();
    return(
        <Formik
            initialValues={{ fname:"",lname:"",phone:"",email: "", password: "",cpassword: "" }}
            onSubmit={(values, { setSubmitting }) => {
                const userData = {};
                userData.fname = values.fname;
                userData.lname = values.lname;
                userData.email = values.email;
                userData.password = btoa(values.password); 
                userData.phone= values.phone;
                localStorage.setItem('user',JSON.stringify(userData));
                history.push('/login');
            }}
            validationSchema = {Validate}
          >
            { props => {
              const {
                values,
                touched,
                errors,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit
              } = props;
                return (
                    <div >
                        <form onSubmit={handleSubmit}>
                            <h3 style={{textAlign:"center"}}>Sign Up</h3>
                            <label htmlFor="email">First Name</label>
                            <input
                            id="fname"
                            name="fname"
                            type="text"
                            placeholder="Enter your first name"
                            value={values.fname}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.fname && touched.fname && "error"}
                            />
                            {errors.fname && touched.fname && (
                            <div className="input-feedback">{errors.fname}</div>
                            )}
                            <label htmlFor="email">Last Name</label>
                            <input
                            id="lname"
                            name="lname"
                            type="text"
                            placeholder="Enter your last name"
                            value={values.lname}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.lname && touched.lname && "error"}
                            />
                            {errors.lname && touched.lname && (
                            <div className="input-feedback">{errors.lname}</div>
                            )}
                            <label htmlFor="email">Email</label>
                            <input
                            id="email"
                            name="email"
                            type="text"
                            placeholder="Enter your email"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.email && touched.email && "error"}
                            />
                            {errors.email && touched.email && (
                            <div className="input-feedback">{errors.email}</div>
                            )}

                            <label htmlFor="password">Password</label>
                            <input
                            id="password"
                            name="password"
                            type="password"
                            placeholder="Enter your password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.password && touched.password && "error"}
                            />
                            {errors.password && touched.password && (
                                <div className="input-feedback">{errors.password}</div>
                            )}
                             <label htmlFor="password">Re enter Password</label>
                            <input
                            id="cpassword"
                            name="cpassword"
                            type="password"
                            placeholder="Enter your confirm password"
                            value={values.cpassword}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.cpassword && touched.cpassword && "error"}
                            />
                            {errors.cpassword && touched.cpassword && (
                                <div className="input-feedback">{errors.cpassword}</div>
                            )}
                            <label htmlFor="email">Contact No</label>
                            <input
                            id="phone"
                            name="phone"
                            type="text"
                            placeholder="Enter your phone no"
                            value={values.phone}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.phone && touched.phone && "error"}
                            />
                            {errors.phone && touched.phone && (
                            <div className="input-feedback">{errors.phone}</div>
                            )}

                            <button style={{float:"right"}} type="submit" className="subBtn" disabled={isSubmitting}>
                            Register
                            </button>
                            <p className="forgot-password text-right">
                                 Already Registerd? <a href={"/login"}>Sign In</a>
                            </p>
                        </form>
                    </div>
                );
            }}
          </Formik>
    )
}
export default Signup