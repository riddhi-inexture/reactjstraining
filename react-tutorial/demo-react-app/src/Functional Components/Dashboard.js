import React , {useState , useEffect} from 'react'
import {Link} from 'react-router-dom';
import Header from '../Functional Components/Header';
import axios from '../api'

function Dashboard() {

    const [ isLoaded ,  setIsLoaded] = useState(false);
    const [ error ,  setError] = useState(null);
    const [ visible ,  setVisible] = useState(3);
    const [ items ,  setItems] = useState([]);

    const loadMore = () => {
        setVisible(visible + 6);
    }
    useEffect(() => {
        axios.get('newsapi.org/v2/everything?domains=wsj.com&apiKey=4f168f74fdef42a99e6c14db1528194c')
        .then(
            res => {
                setIsLoaded(true);
                setItems(res.data.articles);
              
            },
            (error) =>{
                setIsLoaded(true);
                setError(error);
            }
        )
    }, [])
    if(error){
        return <div>Error: {error.message}</div>
    }
    else if(!isLoaded){
        return <div>Loading...</div>
    }
    return(
        <div>  
        <Header/>
        <div style={{textAlign:'center',display:'block',marginBottom:'30px'}}><h3 >Articles</h3></div>
            <div className="container">
                <div className="row" style={{textAlign: "center",position:'relative'}}>
                    
                    {items.slice(0,visible).map(item => (
                        <div className="col-4">
                        <div className="card" style={{width:"22rem"}}>
                        <img src={item.urlToImage} class="card-img-top" alt="..." width="50" height="160"/>
                        <div className="card-body ">
                        <h5 className="card-title"><strong>{item.author}</strong></h5>
                        <p className="card-text">{item.title}</p>
                        <div className="cardBtnDiv">
                        <Link to={{
                                pathname: `/description`,
                                state: {
                                    newsItem : item
                                }
                            }}>
                                <button style={{float:"right", marginBottom:"10px" ,width:"70px",borderRadius:'5px'}} class="load-more">
                                View
                            </button>
                            </Link>
                        {/* <a data-attr={item} href={`/description/${JSON.stringify(item.description)}`} style={{float:"right", marginBottom:"10px" }} class="btn btn-primary ">View</a>  */}
                        </div>
                        </div>
                        </div>
                        
                    </div>
                    ))}

                    {visible < items.length &&
                        <div style={{textAlign:"center"}}><button onClick={loadMore} style={{textAlign:'center',marginLeft:'15px'}} type="button" className="load-more pull-right">Load more</button></div>
                    }
                        
                </div>
            </div>
        </div>
    )


}
export default Dashboard