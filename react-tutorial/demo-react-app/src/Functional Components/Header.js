import React from 'react'
import {useHistory} from 'react-router-dom';

function Header() {
    const history = useHistory();
   const handleClick = () => {
        localStorage.removeItem('user');
        history.push('/');
        window.location.reload();
    }
    let userData = JSON.parse(localStorage.getItem('user'));
    let name = '';
    if(localStorage.getItem('user')){
         name = userData.fname +' '+ userData.lname;
    }            
    else{
        history.push('/');
        window.location.reload();
    }
    return(
        <header className="header-dashboard">
                    <div className="container">
                        <div className="row">
                        <div className="col-6">
                        
                            <h5 className="header-left">
                                Dashboard
                            </h5>
                        </div>
                        <div className="col-6">
                            <ul>
                                <li>
                                {name}
                                </li>
                                <li>
                                <a href={'/dashboard'}>Dashboard</a>
                                </li>
                                <li>
                                <a href={'/pagination'}>Pagination</a>
                                </li>
                                <li>
                                <a href="javascript:void(0)" onClick={handleClick}>Logout</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                        </div>
                        </header>
    )
}
export default Header