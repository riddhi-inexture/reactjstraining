import React from 'react'
import { Formik } from  'formik';
import Validate from '../Validations/LoginValidate'
import { useHistory } from 'react-router-dom';

function Login()
{   
    const history = useHistory();
    var userData;
    return(
      
        <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={(values, { setSubmitting }) => {
            userData = JSON.parse(localStorage.getItem('user'));
            
            console.log(userData);
            if(localStorage.getItem('user')){
                if(userData.email === values.email && atob(userData.password) === values.password){
                    history.push('/dashboard');
                }
                else{
                    // setSubmitting(true);
                    alert('Invalid credentials');
                }
            }
            else{
                // setSubmitting(true);
                alert('Invalid credentials');
            }
        }}       
        validationSchema={Validate}
      >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          
          return (
            <div className="formDiv">
            <form onSubmit={handleSubmit}>
            <h3 style={{textAlign:"center"}}>Log In</h3>
                <label htmlFor="email">Email</label>
                <input
                id="email"
                name="email"
                type="text"
                placeholder="Enter your email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.email && touched.email && "error"}
                />
                {errors.email && touched.email && (
                <div className="input-feedback">{errors.email}</div>
                )}

                <label htmlFor="password">Password</label>
                <input
                id="password"
                name="password"
                type="password"
                placeholder="Enter your password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.password && touched.password && "error"}
                />
                {errors.password && touched.password && (
                    <div className="input-feedback">{errors.password}</div>
                )}

                <button style={{float:"right"}} type="submit" className="subBtn" >
                Login
                </button>
                <p className="forgot-password text-right">
                    Not Yet Registerd? <a href={"/signup"}>Register</a>
                </p>
            </form>
            
            </div>
            
          );
        }}
      </Formik>
    )
}
export default Login