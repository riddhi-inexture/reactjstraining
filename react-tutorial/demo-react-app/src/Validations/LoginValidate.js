import React from 'react'
import * as Yup from 'yup'

const LoginValidates = Yup.object().shape({
    email: Yup.string()
      .email()
      .required("Required"),
    password: Yup.string()
      .required("No password Provided.")
      .min(8, "Password must be of 8 chars minimum.")
      // .matches(/(?=.*[0-9])/, "Password must contain a number.")
  })
  export default LoginValidates;
