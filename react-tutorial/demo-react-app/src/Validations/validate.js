import * as Yup from 'yup'

const phoneRegExp = /^[0-9]{10}$/ 
const Validate = Yup.object().shape({
    fname: Yup.string()
      .required("Required"),
    lname: Yup.string()
      .required("Required"),
    phone: Yup.string()
      .required("Required")
      .matches(phoneRegExp,'Phone number not valid'),
    email: Yup.string()
      .email("valid email")
      .required("Required"),
    password: Yup.string()
      .required("No password provided.")
      .min(8, "Password must be of 8 chars minimum."),
    cpassword: Yup.string()
      .required("No password provided.")
      .min(8, "Password must be of 8 chars minimum.")
      .oneOf([Yup.ref('password'),null],'password must match')
  })
export default Validate